from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from rest_framework.parsers import JSONParser

from single_view.API.serializers import MusicalWorkSerializer
from single_view.API.models import MusicalWork


@csrf_exempt
def list_musical_works(request):
    works = MusicalWork.objects.all()
    serializer = MusicalWorkSerializer(works, many=True)
    return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def get_work_by_iswc(request, iswc):
    works = get_object_or_404(MusicalWork.objects, iswc=iswc)
    serializer = MusicalWorkSerializer(works)
    return JsonResponse(serializer.data, safe=False)
