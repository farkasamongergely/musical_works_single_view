import csv
import logging
import os

from django.core.management.base import BaseCommand
from django.utils import timezone

from single_view.API.models import MusicalWork, Contributor

logger = logging.getLogger("custom")


class Command(BaseCommand):
    help = """
        This command takes a CSV file containing musical work metadata, parses its contents, and then loads it into the database. 
        The command also does basic reconciliation and deduplication.
        Usage: python manage.py ingest_works path/to/input.csv
        """

    @staticmethod
    def parse_works_csv(csv_path):
        works = []  # A list of musical works
        with open(csv_path) as file_handler:
            reader = list(csv.reader(file_handler, delimiter=","))
            colnames = reader[0]
            for row in reader[1:]:
                work = {}  # A single musical work
                for i, col in enumerate(row):
                    colname = colnames[i]
                    if colname == "contributors":
                        col = col.split("|")
                    work[colname] = col
                works.append(work)
        return works

    def add_arguments(self, parser):
        parser.add_argument("csv_path", type=str)

    def handle(self, *args, **kwargs):
        # This function is the entry point when called with manage.py
        # kwargs contains the registered arguments
        # We will handle the error checking and IO from this function

        csv_path = kwargs["csv_path"]
        if not os.path.exists(csv_path):
            logger.error(f"File at {csv_path} does not exist!")
            return

        works = self.parse_works_csv(csv_path)
        logger.info(f"Loading {len(works)} works")
        for i, work in enumerate(works):
            iswc = work.get("iswc")
            title = work.get("title")
            contributors = work.get("contributors")

            if iswc:
                query_by_iswc = MusicalWork.objects.filter(iswc=work["iswc"])
                query_by_title_and_contributor = MusicalWork.objects.filter(
                    title=work["title"], contributors__name__in=contributors
                )
                if query_by_title_and_contributor:
                    work_model = query_by_iswc.union(
                        query_by_title_and_contributor
                    ).first()
                else:
                    work_model = query_by_iswc.first()
            elif title and contributors:
                work_model = MusicalWork.objects.filter(
                    title=work["title"], contributors__name__in=contributors
                ).first()
            else:
                logger.warning(
                    f"Insufficent fields in work, skipping! Parsed work: {work}"
                )
                continue

            if work_model:
                logger.debug(f"Updating work with title: {title}")
                if work_model.iswc != iswc and iswc:
                    work_model.iswc = iswc
                if work_model.title != title and title:
                    work_model.title = title
                for contributor_name in contributors:
                    model_contributor_names = [
                        c.name for c in Contributor.objects.filter(name=contributor_name).all()
                    ]
                    if contributor_name not in model_contributor_names:
                        logger.debug(
                            f"Contributor with name: {contributor_name} does not exists yet. Creating now."
                        )
                        contributor_model = Contributor.objects.create(
                            name=contributor_name
                        )

            else:
                logger.debug(
                    f"Work does not exists yet with title: {title}, ISWC: {iswc}. Creating now."
                )
                work_model = MusicalWork.objects.create(title=title, iswc=iswc)
                for contributor_name in contributors:
                    contributor_model = Contributor.objects.filter(
                        name=contributor_name
                    ).first()
                    if not contributor_model:
                        logger.debug(
                            f"Contributor with name: {contributor_name} does not exists yet. Creating now."
                        )
                        contributor_model = Contributor.objects.create(
                            name=contributor_name
                        )
                    work_model.contributors.add(contributor_model)

            work_model.save()
            logger.info(f"Done saving the model. Progress: {i + 1}/{len(works)}")
