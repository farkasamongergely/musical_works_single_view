from django.db import models


class Contributor(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class MusicalWork(models.Model):
    title = models.CharField(max_length=100)
    # The ISWC standard specifies that the identifier has exactly 11 digits
    iswc = models.CharField(max_length=11)
    contributors = models.ManyToManyField(Contributor)

    class Meta:
        indexes = [models.Index(fields=["iswc"])]

    def __str__(self):
        return self.title


"""
TODO: performance optimizations
https://www.reddit.com/r/django/comments/1z3psf/big_data_solutions_in_django/
https://stackoverflow.com/questions/2051481/django-table-with-million-of-rows
https://docs.djangoproject.com/en/3.2/topics/db/optimization/
https://blog.labdigital.nl/working-with-huge-data-sets-in-django-169453bca049
https://levelup.gitconnected.com/just-one-index-in-django-makes-your-app-15x-faster-742e2f13108e
"""
