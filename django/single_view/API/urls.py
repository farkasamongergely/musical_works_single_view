from django.urls import path
from single_view.API import views

urlpatterns = [
    path("", views.list_musical_works),
    path('<str:iswc>/', views.get_work_by_iswc)
]
