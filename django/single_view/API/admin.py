from django.contrib import admin
from single_view.API.models import MusicalWork, Contributor


class MusicalWorkAdmin(admin.ModelAdmin):
    pass


class ContributorAdmin(admin.ModelAdmin):
    pass


admin.site.register(MusicalWork, MusicalWorkAdmin)
admin.site.register(Contributor, ContributorAdmin)
