from rest_framework import serializers
from single_view.API.models import MusicalWork


class MusicalWorkSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    iswc = serializers.CharField(max_length=11)
    contributors = serializers.StringRelatedField(many=True)
