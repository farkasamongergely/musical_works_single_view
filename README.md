# Musical Works Single View - Demo assignment solution 

This repository contains my solution for the test assignment. Please follow the instructions bellow to install and run the project.

## Installation

### Clone the repository
```bash
git clone https://gitlab.com/farkasamongergely/musical_works_single_view.git
```

### The included dockerfile instructs docker to create 3 containers. One is for the postgresql database, one is for the webserver(nginx) and one hosts the django project. Compose and start the containers with the following commands:

```bash
cd single-view
docker-compose up -d --build
```
### If everything went well, all three containers should be running. You can check this with `docker ps`. 
&nbsp;

### Apply migrations to the database

```bash
docker-compose run django /usr/local/bin/python manage.py migrate 
```

### The metadata CSV is copied to the django container automatically, but we still need to call the ingestion command

```bash
docker-compose run django /usr/local/bin/python manage.py ingest_works works_metadata.csv
```


### Now the project should be accessable from your localhost. Visit http://localhost/works/ to try the API. To query the metadata for a work by ISWC, append it to the end of the URL, like such: http://localhost/works/T9204649558

&nbsp;

# Answers to the questions in the assignment 

1. Describe briefly the matching and reconciling method chosen.

The algorithm uses two functions to clean and store the data in the CSV. The first("parse_works_csv") simply parses the CSV into a data structure(list of dicts), the second(the "handle" function, which is also the entry point for the command) handles the IO, error checking, logging, and the actual reconciling and loading of the data.\
The other parameter, "csv_path" contains a path to the input file. The function first checks if the file exists, then calls parse_works_csv which returns a list of dictionaries. Each dict describes a musical work. If a field in the input CSV was empty, the parsing function simply omits it. We will account for this in the algorithm.\
The function iterates trough the parsed musical works, and extracts the values for the ISWC, title and contributor fields. If either is missing, it sets its value to None.\
If an ISWC was provided, it queries the database for it. It also queries for the title and at least one common contributor. If there is a match, it takes the union of the two queries and stores it in the work_model variable. If there is no match, it uses the result of the first query instead.
If no ISWC was provided, but a title and at least one contributor was, it attempts to query the database for the title and at least one common contributor. It stores the result in the work_model variable.\
If no ISWC was provided, and no title, or no contributor was provided, the function logs an error message and exits. We don't want to deal with such malformed data, because we have no way to identify these in the future.\
If the work_model variable is not empty, that means we got a match from the database for the current work. In this case, we will reconcile the two. We check if the two ISWC values match, if not, we update the ISWC in the model to the ISWC from the input. We do the same for the work title. Then we iterate trough the contributors, and if a contributor doesnt exists in the database yet, we create it.\
If the work_model is empty, that means there was no matching work in the database and we need to create a new object according to the values in the input.\
Lastly, we save the object and log the progress.


2. We constantly receive metadata from our providers, how would
you automatize the process?

We could provide and endpoint in the API for this. Clients could use the API to upload the CSV. The response could contain a job ID, that could be used to poll the progress of the ingest job. Off course this means that we would need to implement a queue, and an endpoint for polling it.\
We could also serve a simple HTML frontend to enable less technical users to interface with the API.

3. Imagine that the Single View has 20 million musical works, do
you think your solution would have a similar response time?

My solution creates an index for the ISWC in the model, so query times should be good enough for reasonably sized datasets. 

4. If not, what would you do to improve it?

If this is not enough, the next step would be to implement caching. The nature of this dataset enables agressive caching as musical work data doesnt change often.

If this is still not enough, we could implement sharding as Postgresql has support for this. Sadly, I dont think the Django ORM supports this, but we could use RawSQL to bypass the ORM.